# Pinlock

## What is this?
This is a gtk ui to a lua virtual machine that happens to look like an unlock screen.

WARNING: This thing can not actually lock your screen and requires the cooperation of at least your window manager!

## Building

### Dependancys

* gtk3
* lua 5.x

* vala
* meson

### How to build

Simply run the build.sh script, it will take care of setting up and invoking the meson build system and put an executable binary called pinlock in the project root folder

The ./run script will call the build.sh script and then automatically execute the produced binary
```
./pinlock -s src/test.lua
```

In case you added, removed, moved or renamed .vala files, running ./update_src_build_files will automatically update the src/meson.build file to include all files in the src folder ending in .vala

## Lua API

running ./pinlock -h should always give an up to date quick documentation of the lua API (you can fild the text that will be printed in the sec/main.vala file)

the test.lua file will also give you an example of how to use the api.
