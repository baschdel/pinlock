public class Pinlock.Widget.Pinlock : Gtk.Box {
	
	public signal void pin_entered(string pin);
	public signal void text_inserted();
	public signal void text_deleted();
	
	private bool setting_pin = false;
	private Gtk.Entry entry = new Gtk.Entry();
	private Gtk.Box button_row_1 = new Gtk.Box(Gtk.Orientation.HORIZONTAL,2);
	private Gtk.Box button_row_2 = new Gtk.Box(Gtk.Orientation.HORIZONTAL,2);
	private Gtk.Box button_row_3 = new Gtk.Box(Gtk.Orientation.HORIZONTAL,2);
	private Gtk.Box button_row_4 = new Gtk.Box(Gtk.Orientation.HORIZONTAL,2);
	private KeypadButton button_1 = new KeypadButton("1");
	private KeypadButton button_2 = new KeypadButton("2");
	private KeypadButton button_3 = new KeypadButton("3");
	private KeypadButton button_4 = new KeypadButton("4");
	private KeypadButton button_5 = new KeypadButton("5");
	private KeypadButton button_6 = new KeypadButton("6");
	private KeypadButton button_7 = new KeypadButton("7");
	private KeypadButton button_8 = new KeypadButton("8");
	private KeypadButton button_9 = new KeypadButton("9");
	private KeypadButton button_0 = new KeypadButton("0");
	private KeypadButton button_null = new KeypadButton("","*");
	private Gtk.Button button_del = new Gtk.Button.from_icon_name("edit-clear-symbolic");
	private Gtk.Button button_enter = new Gtk.Button.from_icon_name("go-jump-symbolic");
	
	public Pinlock(){
		this.orientation = Gtk.Orientation.VERTICAL;
		this.spacing = 2;
		
		entry.input_purpose = Gtk.InputPurpose.PASSWORD;
		entry.set_visibility(false);
		entry.caps_lock_warning = true;
		
		button_row_1.pack_start(button_1);
		button_row_1.pack_start(button_2);
		button_row_1.pack_start(button_3);
		
		button_row_2.pack_start(button_4);
		button_row_2.pack_start(button_5);
		button_row_2.pack_start(button_6);
		
		button_row_3.pack_start(button_7);
		button_row_3.pack_start(button_8);
		button_row_3.pack_start(button_9);
		
		button_row_4.pack_start(button_null);
		button_row_4.pack_start(button_0);
		button_row_4.pack_start(button_del);
		button_row_4.homogeneous = true;
		
		button_del.set_relief(Gtk.ReliefStyle.NONE);
		
		this.pack_start(entry);
		this.pack_start(button_row_1);
		this.pack_start(button_row_2);
		this.pack_start(button_row_3);
		this.pack_start(button_row_4);
		this.pack_start(button_enter);
		
		button_1.append.connect(this.append);
		button_2.append.connect(this.append);
		button_3.append.connect(this.append);
		button_4.append.connect(this.append);
		button_5.append.connect(this.append);
		button_6.append.connect(this.append);
		button_7.append.connect(this.append);
		button_8.append.connect(this.append);
		button_9.append.connect(this.append);
		button_0.append.connect(this.append);
		button_null.append.connect(this.append);
		button_del.clicked.connect(this.backspace);
		button_enter.clicked.connect(this.enter);
		entry.activate.connect(this.enter);
		
		entry.buffer.deleted_text.connect(
			() => {
				if (!setting_pin) {
					text_deleted();
				}
			}
		);
		
		entry.buffer.inserted_text.connect(
			() => {
				if (!setting_pin) {
					text_inserted();
				}
			}
		);
		
		this.show_all();
		
	}
	
	public void append(string text){
		this.entry.buffer.insert_text(this.entry.buffer.length,text.data);
	}
	
	public void backspace(){
		string text = this.entry.text;
		if ( text == "" ){ return; }
		this.entry.buffer.delete_text(this.entry.buffer.length-1,1);
	}
	
	public void enter(){
		this.pin_entered(this.entry.text);
		this.set_pin("");
	}
	
	public void set_pin(string pin){
		setting_pin = true;
		this.entry.text = pin;
		setting_pin = false;
	}
	
	public string get_pin(){
		return this.entry.text;
	}
	
	public void grab_entry_focus(){
		this.entry.grab_focus_without_selecting();
		this.entry.move_cursor(Gtk.MovementStep.BUFFER_ENDS,1,false);
	}
	
}
