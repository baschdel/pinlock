public class Pinlock.Widget.KeypadButton : Gtk.Button {
	
	public signal void append(string text);
	public string append_text;
	
	public KeypadButton(string label, string? append = null){
		this.label = label;
		if (append != null) {
			this.append_text = append;
		} else {
			this.append_text = label;
		}
		this.clicked.connect(this.onclick);
		this.set_relief(Gtk.ReliefStyle.NONE);
	}
	
	private void onclick(){
		this.append(append_text);
	}
}
