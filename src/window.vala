public class Pinlock.Window : Gtk.Window {
	
	public Gtk.Box box;
	
	public Window(){
		box = new Gtk.Box(Gtk.Orientation.VERTICAL,10);
		this.add(box);
		
		this.get_style_context().add_class("pinlock");
		
		this.title = "Pinlock";
		this.border_width = 10;
		this.window_position = Gtk.WindowPosition.CENTER;
	}
	
}
