function unlock()
	display_message("Welcome!")
	--place your unlock code here
	print("Unlocked!")
	os.exit(0)
end

function reset()
	display_message("Please enter your pin!")
	keypad_unlock()
end

function startime()
	show_message("Startime "..os.date("%Y%m%d%H%M")..":\nAccess denied!")
end

handlers = {
	["0000"] = unlock,
	["42"] = "The answer to life is not my pin, nice try!",
	["1234"] = "This is not a counting exercise!",
	["777"] = "The *nix-devils number!",
	["666"] = {"Welcome!", "TO HELL!",delay=2},
	["000"] = startime,
}

insults = {
	"Nope!",
	"No",
	"Not very creative...",
	{"Going boom in 3","Going boom in 2","Going boom in 1","Just kidding",delay=1},
	"Please enter your pin!",
	"Eww, no ...",
	"Please go away",
	"In case you found me please igore the other rude things and call 555-555-555",
	{"...","Still no!",delay=1},
	"HINT: more than two digits",
	"HINT: less than a million digits",
	startime,
}

message = ""
index = 1

function show_message(message)
	if message then
		_G.message = message
		_G.index = 1
	end
	if type(_G.message) == "table" then
		if (_G.message[index]) then
			display_message(_G.message[_G.index])
			_G.index = _G.index + 1
			timeout("show_message",_G.message.delay or 1);
		else
		reset()
		end
	elseif type(_G.message) == "string" then
		display_message(_G.message)
		timeout("reset",2);
	else
		reset();
	end
end

function pin_entered(pin)
	local handler = handlers[pin]
	if not handler then
		handler = insults[((math.random()*#insults)//1)+1] or "blub"
	end
	keypad_lock()
	if type(handler) == "function" then
		handler()
	else
		show_message(handler)
	end
end

function text_inserted()
	print("text_inserted")
end

function text_deleted()
	print("text_deleted")
end

reset()
