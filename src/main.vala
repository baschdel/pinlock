

Pinlock.Window window;
Pinlock.Widget.Pinlock pinlock;
Gtk.Label message_label;

public static int main(string[] args) {
	Gtk.init (ref args);
	
	string scriptfile = GLib.Environment.get_user_config_dir();
	if (!scriptfile.has_suffix("/")) { scriptfile += "/"; }
	scriptfile += "pinlock.lua";
	
	bool next_is_scriptfile = false;
	bool fullscreen = false;
	bool print_help = false;
	string? firstarg = null;
	
	foreach(string arg in args) {
		if (firstarg == null) {
			firstarg = arg;
		} else if (next_is_scriptfile) {
			scriptfile = arg;
			next_is_scriptfile = false;
		} else {
			switch (arg) {
				case "-s":
				case "--scriptfile":
					next_is_scriptfile = true;
					break;
				case "-f":
				case "--fullscreen":
					fullscreen = true;
					break;
				default:	
					print_help = true;
					break;
			}
		}
	}
	
	if (print_help) {
		print(@"Usage: $firstarg [-f] [-s <scriptfile>]
Prompts for a pin and takes actions according to a lua script.
	
With no scriptfile specified the default one is: $scriptfile

  -f, --fullscreen  run in fullscreen mode
  -s, --scriptfile  the next argument is the path to the backend lua script
  
Examples:
  $firstarg -s test.lua  Use the script test.lua as the backend
  $firstarg -f -i        Run in fullscreen mode and ignore quit requests with the default script
  
Lua API:
The script may define the following callbacks:
  pin_entered(string:pin) - called when a pin is entered
  text_inserted() - called when text is inserted into the pin field
  text_deleted() - called when text is deleted from the pin field
  
The script can use the following functions in addition to the lua standard library:
  keypad_lock() locks the keypad
  keypad_unlock() unlocks the keypad
  timeout(string:function_name,number:seconds)
    waits for the specified number of seconds,
    fetches the function of the given name from the global namespace
    and calls it without arguments
  display_message([string:message])
    set the message above the pin entry
  set_pin([string:pin])
  	set the pin in the pin entry field, cleared when nil
  get_pin() string:pin
  	returns the pin currently in the entry field
  	
WARNING: The lua script runs in the same thread as the ui, if the script hangs, the whole application hangs!\n");
		return 0;
	}
	
	window = new Pinlock.Window();
	
	window.destroy.connect(Gtk.main_quit);
	
	if (fullscreen) {
		window.fullscreen();
		window.window_state_event.connect((event) => {
			if (event.new_window_state != FULLSCREEN){
				window.fullscreen();
			}
			return false;
		});
		Timeout.add(500,() => {
			window.fullscreen();
			return true;
		});
	}
	
	message_label = new Gtk.Label("Message label");
	message_label.wrap_mode = Pango.WrapMode.WORD_CHAR;
	message_label.wrap = true;
	window.box.pack_start(message_label);
	
	pinlock = new Pinlock.Widget.Pinlock();
	window.box.pack_start(pinlock);
	
	var luavm = new Lua.LuaVM();
	int res;
	
	luavm.open_libs();
	
	luavm.register("keypad_lock",lock_keypad);
	luavm.register("keypad_unlock",unlock_keypad);
	luavm.register("timeout",timeout);
	luavm.register("display_message",display_message);
	luavm.register("set_pin",set_pin);
	luavm.register("get_pin",get_pin);
	
	res = luavm.load_file(scriptfile);
	if (res != 0) {
		print("[Lua][ERROR][loadfile] "+luavm.to_string(1)+"\n");
		luavm.pop(1);
		return res;
	}
	
	res = luavm.pcall(0,0,0); //0 arg, 0 ret, original error object
	if (res != 0) {
		print("[Lua][ERROR][init] "+luavm.to_string(1)+"\n");
		luavm.pop(1);
	}
	
	pinlock.pin_entered.connect((pin) => {
		luavm.get_global("pin_entered");
		if (luavm.is_nil(-1)) {
			luavm.pop(1);
			return;
		}
		luavm.push_string(pin);
		res = luavm.pcall(1,0,0); //1 arg, 0 ret, original error object
		if (res != 0) {
			print("[Lua][ERROR][pin_entered()] "+luavm.to_string(1)+"\n");
			luavm.pop(1);
		}
	});
	
	pinlock.text_inserted.connect((pin) => {
		luavm.get_global("text_inserted");
		if (luavm.is_nil(-1)) {
			luavm.pop(1);
			return;
		}
		res = luavm.pcall(0,0,0); //0 arg, 0 ret, original error object
		if (res != 0) {
			print("[Lua][ERROR][text_inserted()] "+luavm.to_string(1)+"\n");
			luavm.pop(1);
		}
	});
	
	pinlock.text_deleted.connect((pin) => {
		luavm.get_global("text_deleted");
		if (luavm.is_nil(-1)) {
			luavm.pop(1);
			return;
		}
		res = luavm.pcall(0,0,0); //0 arg, 0 ret, original error object
		if (res != 0) {
			print("[Lua][ERROR][text_deleted()] "+luavm.to_string(1)+"\n");
			luavm.pop(1);
		}
	});
	
	window.key_press_event.connect(
		(e) => {
			pinlock.grab_entry_focus();
			return false;
		}
	);
	
	window.show_all();

	Gtk.main();
	return 0;
}

public static int set_pin(Lua.LuaVM luavm){
	if (!luavm.is_none_or_nil(1)) {
		pinlock.set_pin(luavm.to_string(1));
	} else {
		pinlock.set_pin("");
	}
	return 0;
}

public static int get_pin(Lua.LuaVM luavm){
	luavm.push_string(pinlock.get_pin());
	return 1;
}

public static int lock_keypad(Lua.LuaVM luavm){
	pinlock.sensitive = false;
	return 0;
}

public static int unlock_keypad(Lua.LuaVM luavm){
	pinlock.sensitive = true;
	return 0;
}

public static int timeout(Lua.LuaVM luavm){
	string function = luavm.to_string(1);
	double millis = luavm.to_number(2)*1000;
	luavm.pop(2);
	Timeout.add((uint) millis ,() => {
		luavm.get_global(function);
		var res = luavm.pcall(0,0,0); //0 arg, 0 ret, original error object
		if (res != 0) {
			print("[Lua][ERROR][timeout()] "+luavm.to_string(1)+"\n");
			luavm.pop(1);
		}
		return false;
	},Priority.DEFAULT);
	return 0;
}

public static int display_message(Lua.LuaVM luavm){
	if (luavm.is_none_or_nil(1)) {
		message_label.label = "";
		message_label.visible = false;
	} else {
		message_label.visible = true;
		message_label.label = luavm.to_string(1);
	}
	return 0;
}
